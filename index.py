"""
Title: Practical test 

Create any basic excel or csv file with title, description, amount. Amount should be in number in excel
Open and read the excel or csv file and print it in tabular form with Amount should be in currency format with $ sign.

@author : Sahil Rawat
@email : sahilrawat049@gmail.com
"""

import csv

with open('./assets/readme.csv') as file:
    csv_reader = csv.reader(file, delimiter=',')
    i = 0
    print('Tabular representation of CSV data')
    print('----------------------------------')
    for row in csv_reader:
        if i == 0:
            print(row[0]+'\t'+row[1]+'\t'+row[2])
            print('------------------------------')
        else:
            print(f'{row[0]}\t{row[1]}\t{row[2]}$')
        i += 1
    print(f'----------------------------------')
    print(f'\n\nTotal Number of records {i-1}')